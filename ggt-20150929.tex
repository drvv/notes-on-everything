\documentclass{scrartcl}

\input{../preambles_common.sty}
\input{../drwMacro.sty}
\input{../drwThm.sty}
\input{../drwThmRem.sty} %% show remarks in yellow boxes
\input{../gengauge.sty}
%\usepackage{xcolor} % needed to specify box colour or smt.
%\usepackage{framed} % needed to create leftbar

\usepackage[colorlinks=true]{hyperref}
\usepackage{mathabx}
\makeatletter
\renewcommand{\eqref}[1]{\hyperref[{#1}]{\textup{\tagform@{\ref{#1}}}}}
\makeatother

%% BIB INFO %%
\usepackage[backend=biber]{biblatex}
% can't use style=plain
% can't use \bibliographystyle with biblatex package
% backend should be specified as 'biber'
\bibliography{bourbaki,gengauge} %% must be in the preamble
% \bibliography{bourbaki} %% must be in the preamble
\DefineBibliographyStrings{english}{bibliography={References}} %% "bibliography" for book/report, "references" for others
% %%%%%%%%%%%%%% 

% %% title info %%
\title{Notes on Generalised Gauge Theory \\ -- On Impossibility of Yang-Mills like generalised gauge invariant action -- } %% temporal name
\author{Naoki Shimode}


\begin{document}
\maketitle

In this short report we claim that we cannot constitute the Yang-Mills like generalised gauge invariant action.
\begin{equation}
  \label{eq:def-generalised-yang-mills}
  \int \tr \gencurvature \wedge \hodgestar \gencurvature,
\end{equation}
or more formally,
\begin{equation}
  \label{eq:def-generalised-yang-mills-formal}
  \int \killingwedge{\gencurvature}{\hodgestar\gencurvature}
\end{equation}
where $\gencurvature = \gend \genfield + \half \liewedge{\genfield}{\genfield}$ is the generalised curvature, $\genfield$ is the generalised gauge field, and $\hodgestar$ is the Hodge star operator.

\section{Yang-Mills action and the gauge invariance}
\label{sec:yang-mills-gauge-invariance}

\input{yang-mills-gauge-invariance}

\section{Brief remark on Hodge duality}
\label{sec:brief-remark-on-hodge-duality}

The Hodge star operator could be seen as a purely algebraic map.  Let $\Lambda_r$ be the set of $r$-forms, and $\Lambda$ be the set of all forms on $r$ dimensional manifold.  It is easy to see that
\begin{equation}
  \label{eq:exterior-algebra-dimension}
  \dim \Lambda_r = \dim \Lambda_{n-r} = \begin{pmatrix}n \\ r \end{pmatrix}.
\end{equation}
Therefore the set of $r$-forms and the set of $n-r$-forms are isomorphic.  (No naturalness is discussed because we treat the duality only with algebraic terms here.)
\begin{equation}
  \label{eq:r-form-and-n-r-form-isomorphism}
  \Lambda_r \simeq \Lambda_{n-r}.
\end{equation}
An operator that gives this isomorphism is called \emph{Hodge star operator}.  That is
\begin{align}
  \label{eq:def-hodge-star}
  \hodgestar: \Lambda_r \to \Lambda_{n-r}
\end{align}

\section{Hodge star of generalised fields/parameters}
\label{sec:hodge-star-of-generalised-fields-parameters}

We must define a proper Hodge star operator, based on the notion of Hodge duality described above.  The important step to define Hodge star is clarifying the space where generalised fields $\genfield$ and generalised parameters $\genparam$ live.

To simplify the argument we suppose that generalised fields and parameters are direct sum of odd forms and even forms, respectively, and the dimension of the manifold is odd ($2m+1$).  (Yet this argument applies to the most general case.)  That is,
\begin{align*}
  \genfield &= \field[1] + \field[3] + \dots + \field[2m+1]\\
  \genparam &= \param[0] + \param[2] + \dots + \param[2m]
\end{align*}
Obviously this means
\begin{align*}
  \genfield & \in \Lambda^1 \dsum \dots \dsum \Lambda^{2m+1} \equiv \Lambda^{(1)}\\
  \genparam & \in \Lambda^0 \dsum \dots \dsum \Lambda^{2m} \equiv \Lambda^{(0)}
\end{align*}
Now we note that the dimensions of $\Lambda^{(0)}$ and $\Lambda^{(1)}$ are the same, because each constituent $\Lambda^{k} \subset \Lambda^{(i)} (i \in \ztwo)$ is isomorphic to $\Lambda^{2m+1 - k} \subset \Lambda^{(i+1)}$.

This observation uniquely determines the definition of the Hodge star $\tilde{*}$ of \emph{generalised fields/parameters} (up to isomorphisms).  For example,
\begin{align}
  \label{eq:hodge-star-of-generalised-field-parameter-example}
  \tilde{\hodgestar}\genfield = \tilde{\hodgestar}\paren{\field[1]+\field[3]} = \hodgestar\field[1] + \hodgestar\field[3].
\end{align}
Thus it makes sense that we write $\hodgestar$ for $\tilde{\hodgestar}$.

\section{Generalised Yang-Mills action and the generalised gauge transformation}
\label{sec:generalised-yang-mills-and-the-generalised-gauge-transformation}

The same result \eqref{eq:can-be-generalised-up-until-now} hold for the generalised Yang-Mills action;
\begin{align}
  \delta S &= \int (-1)\killingwedge{\gencurvature}{\liewedge{\hodgestar \gencurvature}{\genparam}} + \killingwedge{\gencurvature}{\hodgestar {\liewedge{\gencurvature}{\genparam}}},
\end{align}
because the variance of the generalised curvature $\gencurvature$ is also only due to the algebraic properties:
\begin{align}
  \label{eq:gencurvature-gauge-transformed}
  \delta \gencurvature &= \liewedge{\gencurvature}{\genparam}.
\end{align}
Now comes the fatal part.  We need
\begin{equation}
  \label{eq:required-relation-for-generalised-yang-mills-to-be-gauge-invariant}
  \hodgestar \liewedge{\gencurvature}{\genparam} = \liewedge{\hodgestar \gencurvature}{\genparam},
\end{equation}
to do the same thing as for the ordinary Yang-Mills action.  The fact that the Hodge star could get inside the bracket and operate only on the curvature crucially depended on the fact that $v$ was $0$-form!  Yet this time we have not only $0$-form but also arbitrary even-forms!  That is
\begin{equation}
  \hodgestar \liewedge{\gencurvature}{\genparam} = \liewedge{\hodgestar \gencurvature}{\genparam},
 \text{ if and only if } \genparam = \param[0].
\end{equation}
There is no way out because we already saw the Hodge star on generalised fields/parameters were uniquely determined through the Hodge star on (ordinary) differential forms.
\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
