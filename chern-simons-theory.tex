
\section{Notations}
\label{sec:chern-simons-notation}
$\mathfrak{g}$: Lie algebra\\
$A$: $\mathfrak{g}$-valued differential $1$-form\\
$T^a$: a generator of $\mathfrak{g}$\\
$T$: a general element of $\mathfrak{g}$\\
$\langle \cdot, \cdot\rangle$: Killing form on $\mathfrak{g}$
%$[\cdot,\cdot]$: 
%Let $A$ be a Lie algebra $\mathfrak{g}$-valued 1-form.
\section{Chern-Simons action}
\label{sec:chern-simons-action}

In most literature in physics, \emph{Chern-Simons action} is written as follows
\begin{equation}
  \label{eq:def-chern-simons-action}
  S[A] = \int \tr \paren{A \wedge dA + \frac{2}{3} A \wedge A \wedge A},
\end{equation}
where the integral is taken over 3-dimensional manifold.

The coefficients differs by authors.  Another convention is
\begin{equation*}
  S[A] = \int \tr \paren{\half A \wedge dA + \frac{1}{3} A \wedge A \wedge A}.
\end{equation*}
This Lagrangian density is called \emph{Chern-Simons 3-form}.

\section{On the definition of Chern-Simons form without matrices}
\label{sec:chern-simons-formal-definition-of-lagrangian-density}

\begin{center}
\fbox{
  \begin{minipage}[h]{0.9\linewidth}
    \paragraph{$\star$Notes  for generalised gauge theory}
    In the papers on generalised gauge theory using graded Lie algebra (e.g.\cite{kawamoto-watabiki-graded} ), we observe some difficulties regarding precise definitions of newly introduced objects:
    \begin{enumerate}
    \item Exponentiation of $\genfield$:  $\genfield^3 \equiv \genfield \wedge \genfield \wedge \genfield$
    \item Trace:  $Tr_*$ or $\text{Htr},\text{Str}$
    \end{enumerate}
    For (1), the space $\genfield^3$ belongs to is not clear, when they insist that they use graded Lie algebra, not an algebra that is closed under multiplication.  For (2), the explicit form of $\tr$ in the generalised CS action is not clearly stated:  They introduced $\text{Htr}$ and $\text{Str}$, mentioning the commutativity they have, however, those operators \emph{were not fully defined} with enough satisfaction.

    We hope that this section clarify the source of the problems stated above, and shows a ``proper way'' to resolve those problems, by considering representation-free description of Chern-Simons action.  %define $\tr$, appearing in generalised CS action.
  \end{minipage}
}
\end{center}

$A$ is Lie algebra valued $1$-form.  Then \emph{is $A \wedge A \wedge A$ Lie algebra valued $3$-form?}  The same for the first term:  \emph{Is $A \wedge dA$ Lie algebra valued}?
% A natural question arises when one looks at the terms appearing in the Lagrangian density of Chern-Simons action \eqref{eq:def-chern-simons-action}:  

A brief answer is; no.  The inside of trace is NOT a Lie algebra valued form.

Suppose $A \wedge A \wedge A$ is Lie algebra valued.  Then for classical Lie algebras such as $\mathfrak{sl}(2,\complex), \mathfrak{su}(2), \dots$, those terms vanish because they do not have abelian subalgebra.

However, Lie algebras in general does not have \emph{multiplication} defined in it.  Then what does that term mean?  How is it defined?  Does Chern-Simons theory require matrix representation, or universal enveloping algebra?

There are two keys to solve this problem.  One is \emph{wedge of Lie algebra valued forms}, and the other is \emph{the Killing form.}

\subsection{Wedge of Lie algebra valued forms -- How naive operation fails}
\label{sec:chern-simons-wedge-of-lie-algebra-how-fails}

First of all, in the easiest case, we should recognise that we cannot compose Lie algebra valued $2$-form out of its $1$-forms.  Let $A,B$ be Lie algebra valued $1$-forms.  Using a local coordinate system, they look
\begin{equation*}
  A = A^a_\mu T^a \tensor dx^\mu, \quad  B = B^b_\nu T^b \tensor dx^\nu,
\end{equation*}
where $T^a (a=1,\dots,\dim{\mathfrak{g}})$ are generators of the algebra $\mathfrak{g}$.  We omit tensor product symbol in what follows.  The wedge product of these should be
\begin{equation*}
  A \wedge B = A^a_\mu B^b_\nu T^a T^b dx^\mu \wedge dx^\nu.
\end{equation*}
The problem here is that we need to define multiplication in $\mathfrak{g}$ in some way, for $T^a T^b$ to make sense, or we need to write $T^a T^b$ only in terms of Lie bracket of $\mathfrak{g}$.  Actually, this can be done when $A = B$;
\begin{align}
  A \wedge A &= A^a_\mu A^b_\nu T^a T^b dx^\mu \wedge dx^\nu\\
  &= \half \paren{A^a_\mu A^b_\nu T^a T^b dx^\mu \wedge dx^\nu + A^b_\nu A^a_\mu T^b T^a dx^\nu \wedge dx^\mu}\\
  \label{eq:lie-algebra-valued-wedge-naive-rearrange}
  &= \half A^a_\mu A^b_\nu (T^a T^b - T^b T^a) dx^\mu \wedge dx^\nu\\
  &= \half \Liebra{A_\mu}{A_\nu} dx^\mu \wedge dx^\nu.
\end{align}
The reason why it worked fine was that we had \emph{antisymmetry} or \emph{graded commutation relation} for wedge $\wedge$ and the fact that coefficients $A^a_\mu, A^b_\nu$ coincided :  Otherwise the rearrangement of summation will not lead to the commutation of generators in the third line \eqref{eq:lie-algebra-valued-wedge-naive-rearrange}.

Note that this kind manipulation will never work when we consider $A \wedge A \wedge A$, because then \emph{graded commutation} does not give signs:
\begin{equation*}
  \dx[\mu] \wedge \paren{\dx[\nu] \wedge \dx[\rho]} = \paren{\dx[\nu] \wedge \dx[\rho]} \wedge \dx[\mu].
\end{equation*}

\subsection{Wedge of Lie algebra valued forms -- true definition}
\label{sec:chern-simons-wedge-of-lie-algebra-true}

This ill-definedness of wedging is a severe problem for this theory.  Mathematical literature \emph{defines} wedging of Lie algebra valued forms slightly differently from naive expectation.
\begin{definition}[Wedge product of Lie algebra valued differential forms]
  Let $\omega, \eta$ be Lie algebra valued $p,q$-form, respectively.  Namely, $\omega,\eta: T_pM \to \mathfrak{g}$.  Let $X_1,\dots,X_{p+q}$ be tangent vectors at $p \in M$.  The \emph{wedge} of $\omega$ and $\eta$ is\footnote{Some authors prefer to put a coefficient such as $\frac{1}{(p+q)!}$ and $\frac{p!q!}{(p+q)!}$ in front of the summation.}
  \begin{equation}
    \label{eq:def-wedge-of-lie-algebra-valued-forms}
    [\omega \wedge \eta] (X_1,\dots,X_{p+q}) := \sum_{\sigma \in \mathfrak{S}_{p+q}} \sign{\sigma} \Liebra{\omega(X_{\sigma(1)},\dots,X_{\sigma(p)})}{\eta(X_{\sigma(p+1)},\dots,X_{\sigma(p+q)})}.
  \end{equation}
\end{definition}
Note that Lie bracket is explicitly taken, as the notation suggests:\footnote{Some authors write $[\omega , \eta]$; without a wedge symbol.  But I have never encountered a notation without bracket [,].}  $[\omega \wedge \eta]$ is Lie algebra valued $(p+q)$-form \emph{by construction.}

In order to make this definition familiar, let us calculate a very special case; where $\omega = \eta = A$.  Since $A$ is $1$-form, $[A\wedge A]$ takes two tangent vectors.
\begin{align*}
  [A \wedge A] (X_1,X_2) &:= \sum_{\sigma \in \mathfrak{S}_{2}} \sign{\sigma} \Liebra{A(X_{\sigma(1)})}{A(X_{\sigma(2)})}\\
  &= (-1)^0 \Liebra{A(X_{1})}{A(X_{2})} + (-1)^1 \Liebra{A(X_{2})}{A(X_{1})}\\
  &= \Liebra{A(X_1)}{A(X_2)}.\\
  \intertext{Further, with matrix representation}
  &= A(X_1)A(X_2) - A(X_2)A(X_1)\\
  &= 2(A \wedge A)(X_1,X_2).
\end{align*}
Thus we conclude\footnote{As seen in the calculation, different conventions for coefficients in the definition of wedge modifies this result.}\footnote{This might be why the mathematical literature often writes the curvature form $F$ as
\begin{equation*}
  F = dA + \half [A \wedge A].
\end{equation*}}
\begin{equation}
  \label{eq:lie-algebra-valued-wedge-of-same-one-form-half-of-proper-wedge}
  A \wedge A = \half [A \wedge A].
\end{equation}
In general, using matrix representation,
\begin{equation}
  \label{eq:lie-algebra-valued-wedge-matrix-representation-in-general}
  [\omega \wedge \eta] = \omega \wedge \eta - (-1)^{pq} \eta \wedge \omega.
\end{equation}
We have translated $A \wedge A$ into the formal language.  What about $A \wedge A \wedge A$, then?  Does the same game apply?

Again, the answer is no.  For instance, $[A \wedge [A \wedge A]]$ should, by construction, be Lie algebra valued $3$-form.  However, when you start calculating, you would realise that it gives \emph{zero.}

\subsubsection{Killing form -- the origin of trace}
\label{sec:chern-simons-killing-form}

\begin{definition}[Killing form]
  \emph{Killing form} $\langle , \rangle$ is a bilinear symmetric form $\mathfrak{g} \times \mathfrak{g} \to \complex$ on Lie algebra.  The explicit form is
  \begin{equation}
    \label{eq:def-killing-form}
    \killing{T}{T'} = \tr \sqbra{\adj(T_1) \circ \adj(T_2)}.
  \end{equation}
\end{definition}
We do not go into details of Killing form here, only recalling that this satisfies \emph{invariant property}:
\begin{equation}
  \label{eq:killing-form-invariance}
  \killing{T}{\Liebra{T'}{T''}} = \killing{\Liebra{T}{T'}}{T''}.
\end{equation}
This property turns out to ensure the associativity of $A \wedge A \wedge A$.

Finally, Killing form on Lie algebra valued forms is defined as
\begin{equation}
  \label{eq:def-killing-form-on-lie-algebra-valued-forms}
  \killingwedge{A_1}{A_2} := A_1^a \wedge A_2^b \killing{T^a}{T^b}.
\end{equation}

\subsubsection{Representation-free expression of Chern-Simons $3$-form -- the conclusion}
\label{sec:chern-simons-formal-definition-of-lagrangian-density-conclusion}

We are ready to write Chern-Simons action without using matrix representation.  It writes
\begin{equation}
  \label{eq:chern-simons-action-without-representation}
  \int \killingwedge{A}{dA} + \frac{2}{3} \killingwedge{A}{\liebrawedge{A}{A}}.
\end{equation}


\section{Gauge invariance}
\label{sec:chern-simons-gauge-invariance}

Gauge transformation is given by
\begin{equation}
  \label{eq:def-gauge-transformation}
  \delta A = dv + \comm{A}{v}
\end{equation}
where $v$ is ``gauge parameter''. %% discussion is needed.  What is a gauge parameter?  Fibre parameter?

\newcommand{\saa}{\second}      %requires 'mathabx'
\input{../research/calc/variance-cs-action}

\begin{center}
  \it to be continued
\end{center}



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "research-progress.tex"
%%% End: 
