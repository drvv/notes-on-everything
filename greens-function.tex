
\section{Green's function}
\label{sec:greens-function}

Green's function is a function that is used in solving linear partial differential equation (LPDE).  Namely it gives particular solution for LPDE. Therefore the Green's function method is widely applied to physics:  Radiation of electromagnetic wave, diffusion of heat, and propagation of particles in quantum field theory are analysed via computing Green's function.

In general, Green's function is not a ``function'', that is, hyperfunction or distribution.  Such mathematical treatment is not mentioned in this article.

\subsection{Definition}
\label{sec:def-greens-function}

Let $\mathcal{L}$ be a linear differential operator, and consider a inhomogeneous linear partial differential equation such as
\begin{equation}
  \label{eq:greens-function-inhomogeneous-equation}
  \mathcal{L}(\phi(x)) = \rho(x),
\end{equation}
where $\phi(x)$ is a function to solve, and $\rho(x)$ is a given function, which is called \emph{source term} for this equation.

Green's function is determined for each linear partial differential equation.  Green's function for this equation is defined as the function which satisfies the following equation.
\begin{equation}
  \label{eq:def-greens-function}
  \mathcal{L}(G(x,x_0)) = \delta(x-x_0).
\end{equation}
In other words, Green's function is the solution to the equation \emph{whose source term is Dirac delta}.

Suppose you found Green's function, that is, you solved \eqref{eq:def-greens-function}.  Then it follows that you can construct particular solution for the equation.  To see this, multiply by $\rho(x_0)$ and integrate the both sides with respect to $x_0$ gives
\begin{align*}
  \int dx_0 \, \rho(x_0) \mathcal{L}(G(x,x_0)) &= \int dx_0 \, \rho(x_0) \delta(x-x_0)\\
  \mathcal{L} \paren{ \int dx_0 \, \rho(x_0) G(x,x_0) } &= \rho(x).
\end{align*}
This result tells us that the inside of the parentheses
\begin{equation}
  \label{eq:greens-function-fundamental-solution}
  \int dx_0 \, G(x,x_0) \rho(x_0)
\end{equation}
is a solution to the original equation.  This is called \emph{fundamental solution} to the differential equation.  Note that the linearity of $\mathcal{L}$ is used above.

A lot of literature introduces the fundamental solution in the reversed order.  They first introduce such function
\begin{equation}
  \label{eq:greens-function-fundamental-solution}
  \phi_0 = \int dx_0 \, G(x,x_0) \rho(x_0).
\end{equation}
and then verify that this is indeed a solution.  Applying the linear operator $\mathcal{L}$ gives
\begin{align*}
  &\mathcal{L}(\phi_0(x)) \\
  &= \mathcal{L}\paren{\int dx_0 \, G(x,x_0) \rho(x_0)}\\
  &= \int dx_0 \, \mathcal{L}(G(x,x_0) \rho(x_0))\\
  &= \int dx_0 \, \mathcal{L}(G(x,x_0)) \rho(x_0)\\
  &= \int dx_0 \, \delta(x-x_0) \rho(x_0)\\
  &= \rho(x).
\end{align*}
Note again that the linearity of $\mathcal{L}$ is used above.

\subsection{Implications}
\label{sec:greens-function-implication}

\subsubsection{Algebraic}
\label{sec:greens-function-implication-algebraic}

Sometimes Green's function is said to be the \emph{inverse} of the linear differential operator $\mathcal{L}$.  Look at the defining equation of Green's function, and consider differential operator $\mathcal{L}$ and the Green's function $G(x,x_0)$ as ``matrices'', whose labels are continuous.  The equation is an analogue to the finite linear algebraic case.
\begin{equation}
  \label{eq:greens-function-finite-analogue}
  \mathcal{L} G_{x,x_0} = \delta_{x,x_0}.
\end{equation}
This implies
\begin{equation}
  \label{eq:gcreens-function-as-inverse-in-realspace}
  G = \mathcal{L}^{-1}.
\end{equation}

The method of Fourier transform makes this situation clearer.  The differential operator $\mathcal{L}$ contains some differential operators and it becomes multiplication operator when Fourier transformed:
\begin{equation*}
  \mathcal{L}[\partial] \to \mathcal{L}[ik].
\end{equation*}
Now the linear differential equation becomes
\begin{equation}
  \label{eq:greens-function-fourier-transformed}
  \mathcal{L}[ik] \tilde{G} = 1
\end{equation}
This time $\mathcal{L}$ only contains multiplication.  Therefore it can simply be \emph{inverted}:
\begin{equation}
  \label{eq:greens-function-as-inverse-in-fourier-space}
  \tilde{G} = \frac{1}{\mathcal{L}[ik]}.
\end{equation}

\subsubsection{Physical}
\label{sec:greens-function-implication-physical}
\newcommand{\pos}{\bm{r}}
We did not mention why $\rho(x)$ is called \emph{source} term.  To see this, we focus on electromagnetic theory.  Scalar potential $\phi$ satisfies Poisson's equation.
\begin{equation}
  \label{eq:poissons-eq-electrostatics}
  \Laplacian \phi(\pos) = -\frac{\rho(\pos)}{\epsilon_0},
\end{equation}
where $\rho(\pos)$ is the charge distribution and $\epsilon_0$ the vacuum permittivity.

Recall that electromagnetic field admits \emph{superposition principle}:  Given two charges at each points, the electric fields that are created in total, is simply the sum of each charge's contribution.  In terms of potential, the scalar potential simply adds:
\begin{equation*}
  \phi(x) = \phi_1(\pos) + \phi_2(\pos),
\end{equation*}
where $\phi_{1}(\pos), \phi_{2}(\pos)$ are the contributions from the point charge located at $\pos_1,\pos_2$, respectively.  Therefore in order to determine the value of the field in the total system, we only need to find each contribution $\phi_i$:
\begin{equation*}
  \Laplacian \phi_i(\pos) = -\frac{\delta(\pos-\pos_i)}{\epsilon_0}
\end{equation*}
We assumed that sources are point charges.  But this idea naturally generalises to the case of continous charge distribution, because such distributions can be regarded as a sum of point charges anyway.  This idea is represented by the following trivial identity.
\begin{equation*}
  \rho(\pos) = \int dV \rho(\pos') \delta(\pos-\pos')
\end{equation*}
This reads ``the entire distribution is the collection of all tiny point charges $dV \rho(\pos') \delta(\pos-\pos')$''.  See the figure below \ref{fig:charge-density-as-point-charges}.
\begin{figure}[b]
  \centering
  \includegraphics[width=6cm]{collection-of-point-charges}
  \caption{Charge density as a sum of point charges}
  \label{fig:charge-density-as-point-charges}
\end{figure}

In this perspective, solving general Poisson's equation \eqref{eq:poissons-eq-electrostatics} is reduced to the case of single point charge having $+1$ coulomb:
\begin{equation*}
  \Laplacian \phi(\pos,\pos') = -\frac{\delta(\pos-\pos')}{\epsilon_0}.
\end{equation*}
The potential $\phi(\pos,\pos')$ appearing here is the one that is created by $+1$ coulomb that is located at $\pos=\pos'$.  This is exactly the \emph{Green's function for Poisson's equation}, other than the coefficient before $\delta$, which is $-\frac{1}{\epsilon_0}$.

\subsubsection{As propagator}
\label{sec:greens-function-propagator}


\subsubsection{Relation to Huygens principle}
\label{sec:greens-function-huygens-principle}



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "master"
%%% End: 
