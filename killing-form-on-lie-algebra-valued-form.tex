This section summarises the algebraic aspects of the Killing form on Lie algebra valued differential forms.  

We suppose that a Killing form $\langle \ , \ \rangle$ is defined in a Lie algebra $\liealg$.

We need to \emph{define} Killing form $\langle \ \wedge\  \rangle$ \emph{on $\liealg$-valued differential forms} $\liealg \tensor \Lambda$.

\subsection{Definition of wedged Killing form}
\label{sec:def-wedged-killing-form}

The definition is a natural extension of the original Killing form.  It takes $\liealg$-valued form and the output is a differential form, whose coefficient is the Killing form.

%Notation:  $\tilde{A} = A \tensor \alpha \in \liealg \tensor \Lambda$.

%\newcommand{\killingwedge}[2]{\left \langle #1 \wedge #2 \right \rangle}
\newcommand{\gwedge}[2]{\sqbra{ #1 \wedge #2 }}

\begin{definition}[Killing form on $\liealg$-valued differential forms, wedged Killing form]
  Let $A = a \tensor \alpha, B = b \tensor \beta \in \liealg \tensor \Lambda$.  A binary map $\liealg \tensor \Lambda \to \Lambda$ can be defined as
%  Suppose $\tilde{A}$ and $\tilde{B}$ are in a local form.  Define
  \begin{equation}
    \label{eq:def-killing-wedge}
    \killingwedge{A}{B} = \killingwedge{a}{b} \alpha \wedge \beta.
  \end{equation}
  We call the bilinear extension of this operator a \emph{wedged Killing form} in the following.
\end{definition}

\subsection{Algebraic properties of wedged Killing form}
\label{sec:killing-form-with-wedge-algebraic}

The following table summarises the content of this section.  We make a contrast with the original Killing form.
\begin{table}[h]
  \centering
  \begin{tabular}[h]{r || c | c}
    space & $\liealg$ & $\liealg \tensor \Lambda$\\
    operation & $\killing{\ }{\ }$ & $\killingwedge{\ }{\ }$ \\
    symmetry &
    $\begin{matrix}
      \text{symmetric}\\
      \killing{A}{B} = -\killing{B}{A}
    \end{matrix}$
    & $
    \begin{matrix}
      \text{\emph{graded} symmetric}\\
      \killingwedge{A}{B} = \parityfactor{A}{B}\killingwedge{B}{A}
    \end{matrix}$ \\
    invariance & $\killing{A}{\liebra{B}{C}} = \killing{\liebra{A}{B}}{C}$ & $\killingwedge{{A}}{\liewedge{{B}}{{C}}} = \killingwedge{\liewedge{{A}}{{B}}}{{C}}$
  \end{tabular}
  \caption{Algebraic properties of wedged Killing form}
  \label{tab:algebraic-properties-of-wedged-killing}
\end{table}

% We now make a contrast of the properties with the ones owned by the original Killing form.

\subsubsection{Graded symmetry}
\label{sec:killing-form-with-wedge-algebraic-graded-symmetry}

While the original Killing form is symmetric;
\begin{equation*}
  \killing{A}{B} = \killing{B}{A},
\end{equation*}
the wedged Killing form is \emph{graded symmetric};
\begin{proposition}[Graded symmetry of wedged Killing form]
  \begin{equation}
    \label{eq:lie-algebra-valued-killing-graded-symmetry}
    \killingwedge{{A}}{{B}} = \parityfactor{A}{B} \killingwedge{{B}}{{A}}.
  \end{equation}
\end{proposition}
This is easily shown by naive calculations.

\subsubsection{Invariance}
\label{sec:killing-form-with-wedge-algebraic-invariance}

While the original Killing form is invariant;
\begin{equation}
  \label{eq:killing-form-invariance}
  \killing{A}{\liebra{B}{C}} = \killing{\liebra{A}{B}}{C},
\end{equation}
the wedged Killing form is also \emph{invariant} with respect to $\liealg$-valued wedge $\liewedge{\ }{\ }$.
\begin{proposition}[The invaliance of wedged Killing form]
  \begin{equation}
    \label{eq:lie-algebra-valued-killing-invariance}
    \killingwedge{{A}}{\liewedge{{B}}{{C}}} = \killingwedge{\liewedge{{A}}{{B}}}{{C}}
  \end{equation}
\end{proposition}
\begin{proof}
  \begin{align}
    \label{eq:proof-wedged-killing-invariance}
    \killingwedge{A}{\liewedge{B}{C}}    &= \killing{a}{\liebra{b}{c}} \tensor (\alpha \wedge \beta \wedge \gamma)\\
    &= \killing{\liebra{a}{b}}{c} \tensor (\alpha \wedge \beta \wedge \gamma)\\
    &= \killingwedge{\liewedge{A}{B}}{C}
  \end{align}
\end{proof}

\subsubsection{Leibniz rule for wedged Killing form}
\label{sec:killing-form-with-wedge-algebraic-leibniz}

Leibniz rule is easily shown by simple calculations.
\begin{proposition}[Leibniz rule for wedged Killing form]
\begin{equation}
  \label{eq:wedged-killing-leibniz-rule}
  \extd \killingwedge{A}{B} = \killingwedge{\extd A}{B} + (-1)^{\abs{A}} \killingwedge{A}{\extd B}.
\end{equation}  
\end{proposition}
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "notes-on-ggt-20150731.tex"
%%% End: 
