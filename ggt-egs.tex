
\subsection{Differential forms only}
\label{sec:ggt-egs-diffform-only}

The minimal setup might be a theory only with ``ordinary'' differential forms.  That is, no tensoring other chain complexes such as fermions.

\renewcommand{\field}{A}
\renewcommand{\param}{v}
\begin{align}
  \label{eq:eg-only-diff-form}
  \genfield &= \field^1 + \field^3 + \field^5 + \dots \\
  \genparam &= \param^0 + \param^2 + \param^4 + \dots\\
  \genextd &= \extd
\end{align}

This setup could still be interesting in the context of \emph{$p$-form electromagnetism}, and consequently the theory of \emph{D-branes}, where higher forms are considered to be \emph{Ramond-Ramond fields}.  This is because non-abelian theory of $p$-form electromagnetism, and consequently the theory of D-branes is not yet discovered, although some authors try to do this.  [ref]  We could expect that our theory can be interpreted in terms of those theories.

In what follows we give an very explicit example of generalised CS action, the equation of motion derived from it, and its non-abelian gauge transformation of them.

\subsubsection{In three dimension}
\label{sec:ggt-egs-diffform-only-dim-three}

Consider three dimensional manifold without boundary.  The maximal content of the generalised field and parameters are
 \renewcommand{\field}[1][\mbox{}]{A^{#1}}
% \newcommand{\rrfield}[1][\mbox{}]{C^{#1}}
 \renewcommand{\param}[1][\mbox{}]{v^{#1}}
% -> gengauge.sty
\begin{align}
  \label{eq:eg-only-diff-form-dim-three}
  \genfield &= \field[1] + \rrfield[3]\\
  \genparam &= \param[0] + \param[2]
\end{align}
Let us find the generalised CS action in terms of those components $\field[1], \rrfield[3]$.  The generalised CS action now writes
\begin{equation}
  \label{eq:eg-only-diff-form-generalised-CS}
  S = \int_3 \killingwedge{{\field[1] + \rrfield[3]}}{\extd \paren{\field[1] + \rrfield[3]}} + \frac{1}{3} \killingwedge{\paren{\field[1]+\rrfield[3]}}{\liewedge{\field[1]+\rrfield[3]}{\field[1]+\rrfield[3]}},
\end{equation}
where $\int_3$ indicates that the integration is done in some three dimensional manifold.

The higher forms than $3$-form, such as $\extd \rrfield[3], \rrfield[3]\wedge \field[1]$ in this integration vanishes.  In particular, the highest form $\rrfield[3]$ never appears in the final form of action:
\begin{equation}
  \label{eq:eg-only-diff-form-generalised-CS-in-components}
  S_3 = \int_3 \killingwedge{\field[1]}{\extd \field[1]}  + \frac{1}{3} \killingwedge{\field[1]}{\liewedge{\field[1]}{\field[1]}}.
\end{equation},
or with matrix representation,
\begin{equation}
  \label{eq:eg-only-diff-form-generalised-CS-in-components-matrep}
  S_3 = \int_3 {\field[1]} \wedge {\extd \field[1]}  + \frac{2}{3} {\field[1]} \wedge {{\field[1]} \wedge {\field [1]}}.
\end{equation}
This is exactly the well known Chern-Simons action itself.  This fact shows our construction is consistent with original CS theory in the minimal setting.  In other words, our ``generalisation'' is indeed a generalisation, or extension of the original theory.

\subsubsection{In four dimension}
\label{sec:ggt-egs-diffform-only-dim-four}

As you might realise during the calculation above, no terms can constitute $4$-forms.  Therefore the integral vanishes.  Our theory which \emph{only use differential forms} does not have any implications in four dimension.  One possible remedy is to use \emph{fermions}.  We will see this example in the next section \ref{sec:ggt-egs-diff-form-times-fermions}.

\subsubsection{In five dimension}
\label{sec:ggt-egs-diffform-only-dim-five}

The first nontrivial result is obtained in five dimensional space.  Now the terms such as $\field[1] \wedge \extd \rrfield[3], \field[1]\wedge\field[1]\wedge\rrfield[3]$ survive.  The straightforward calculation gives
\begin{equation}
  \label{eq:eg-only-diff-form-generalised-CS-in-components-in-five-dimension}
  S_5 = \int_5 \killingwedge{\field[1]}{\extd \rrfield[3]} + \killingwedge{\rrfield[3]}{\extd \field[1]} + \killingwedge{\liewedge{\field[1]}{\field[1]}}{\rrfield[3]}.
\end{equation}
Although our notation is algebraically simpler when manipulating terms correctly, the traditional matrix representation of this type of action might help understanding.  With matrix representation of the underlying Lie algebra, we have
\begin{equation}
  \label{eq:eg-only-diff-form-generalised-CS-in-components-in-five-dimension-matrep}
  S_5 = \int_5 {\field[1]}\wedge {\extd \rrfield[3]} + {\rrfield[3]} \wedge {\extd \field[1]} + 2 {{\field[1]} \wedge {\field[1]}} \wedge {\rrfield[3]}.
\end{equation}

\paragraph{Equations of motion}

The equation of motion for generalised field
\begin{equation}
  \label{eq:eg-eom}
  \gencurvature = \genextd \genfield + \half \liewedge{\genfield}{\genfield} = 0
\end{equation}
gives the equation of motions for each field components $\field[1], \rrfield[3]$.
\begin{align}
  \label{eq:eg-eom-in-components-calc}
  \extd  \paren{\field[1] + \rrfield[3] + \rrfield[5]} + \half \liewedge{\paren{\field[1] + \rrfield[3] + \rrfield[5]}}{\paren{\field[1] + \rrfield[3] + \rrfield[5]}}&=0\\
  \extd \field[1] + \half \liewedge{\field[1]}{\field[1]} + \extd \rrfield[3] + \half \liewedge{\field[1]}{\rrfield[3]} + \half \liewedge{\rrfield[3]}{\field[1]} &= 0.
\end{align}
Therefore the curvature forms, and eoms for each component write
\begin{align}
  \label{eq:eg-eom-in-components}
  \curvatureform_2 &= \extd \field[1] + \half \liewedge{\field[1]}{\field[1]} = 0,\\
  \curvatureform_4 &= \extd \rrfield[3] +  \liewedge{\rrfield[3]}{\field[1]} = 0,
\end{align}
or using matrix representation
\newcommand{\matrepliewedge}[2]{#1 \wedge #2}
\begin{align}
  \label{eq:eg-eom-in-components}
  \curvatureform_2 &= \extd \field[1] + \matrepliewedge{\field[1]}{\field[1]} = 0,\\
  \curvatureform_4 &= \extd \rrfield[3] + \matrepliewedge{\rrfield[3]}{\field[1]} + \matrepliewedge{\field[1]}{\rrfield[3]} = 0\\
  \intertext{or with the sign rule is understood,}
  & \extd \field[1] + \half \liebra{\field[1]}{\field[1]} = 0,\\
  & \extd \rrfield[3] + \liebra{\rrfield[3]}{\field[1]} = 0.
\end{align}
Using these field strengths, we can rewrite the action in the following way.
\begin{equation}
  \label{eq:eg-only-diff-form-generalised-CS-in-components-in-five-dimension-with-fieldstr}
  S_5[\field[1], \rrfield[3]] = \int_5 \tr\paren{\curvature_2 \wedge \rrfield[3] + \curvature_4 \wedge \field[1] - \field[1] \wedge \field[1] \wedge \rrfield[3]}.
\end{equation}

\paragraph{Gauge transformation}

The generalised gauge transformation
\begin{equation}
  \label{eq:eg-gauge-transformation}
  \delta \genfield = \genextd \genfield + \liewedge{\genfield}{\genparam}
\end{equation}
derives the gauge transformations for each component $\field[1], \rrfield[3]$.  In five dimension, we generally have the following transformation (including the transformation for $5$-form).
\begin{align}
  \label{eq:eg-gauge-transformation-five-dimension}
  \delta \genfield &= \extd \paren{\param[0] + \param[2] + \param[4]} + \liewedge{\field[1] + \rrfield[3] + \rrfield[5]}{\param[0] + \param[2] + \param[4]}\\
  &= \extd \param[0] + \liewedge{\field[1]}{\param[0]} \\
  &+ \extd \param[2] + \liewedge{\field[1]}{\param[2]} + \liewedge{\rrfield[3]}{\param[0]} \\
  &+ \extd \param[4] + \liewedge{\field[1]}{\param[4]} + \liewedge{\rrfield[3]}{\param[2]} + \liewedge{\rrfield[5]}{\param[0]}
\end{align}
Therefore, the gauge transformations for each components $\field[1], \rrfield[3]$ are
\begin{align}
  \label{eq:eg-gauge-transformation-five-dimension-in-components}
  \delta \field[1] &= \extd \param[0] + \liewedge{\field[1]}{\param[0]},\\
  \delta \rrfield[3] &= \extd \param[2] + \liewedge{\field[1]}{\param[2]} + \liewedge{\rrfield[3]}{\param[0]},
\end{align}
or using matrix representation,
\renewcommand{\matrepliewedge}[2]{\liebra{#1}{#2}}
\begin{align}
  \label{eq:eg-gauge-transformation-five-dimension-in-components-matrep}
  \delta \field[1] &= \extd \param[0] + \matrepliewedge{\field[1]}{\param[0]},\\
  \delta \rrfield[3] &= \extd \param[2] + \matrepliewedge{\field[1]}{\param[2]} + \matrepliewedge{\rrfield[3]}{\param[0]}.
\end{align}
Although the explicit calculation is not provided here, the action written in components is invariant under these transformations.


% Here is the summary of this section (in five dimensional space).
% \begin{center}
%   \fbox{
%     \begin{minipage}{1\linewidth}
%       \paragraph{Action for $\field[1], \rrfield[3]$}    
% %      The action for $\field[1]$ and $\rrfield[3]$
%       \begin{equation*}
%         S_5[\field[1], \rrfield[3]] = \int_5 \tr \paren{\field[1]\wedge {\extd \rrfield[3]} + {\rrfield[3]} \wedge {\extd \field[1]} + 2 {{\field[1]} \wedge {\field[1]}} \wedge {\rrfield[3]}}.  
%       \end{equation*}

%       \paragraph{Field strength and equations of motions  for $\field[1], \rrfield[3]$}
% %      gives the equations of motions for each field
%       \begin{align*}
%         \curvature_2 &\equiv \extd \field[1] + \half \matrepliewedge{\field[1]}{\field[1]} = 0,\\
%         \curvature_4 &\equiv\extd \rrfield[3] + \matrepliewedge{\rrfield[3]}{\field[1]} = 0
%       \end{align*}

%       \paragraph{Gauge invariance}
% %      and they are invariant under the gauge transformation
%       \begin{align*}
%         \delta \field[1] &= \extd \param[0] + \matrepliewedge{\field[1]}{\param[0]},\\
%         \delta \rrfield[3] &= \extd \param[2] + \matrepliewedge{\field[1]}{\param[2]} + \matrepliewedge{\rrfield[3]}{\param[0]}.
%       \end{align*}
%       with the sign rule
%       \begin{equation*}
%         \comm{\rrfield[i]}{\rrfield[j]} = \rrfield[i] \wedge \rrfield[j] + (-1)^{ij} \rrfield[j] \wedge \rrfield[i]
%       \end{equation*}
%     \end{minipage}
%   }
% \end{center}
The same argument applies to higher dimensional spaces and there we have as much higher form as the dimension.  We only list some examples in the following, without explanations.
\subsubsection{In seven dimension}
\label{sec:ggt-egs-diffform-only-dim-seven}

%% seven dimensional example
The choice here is
%seven dimensional choise
\begin{align*} 
\genfield = \field[1] + \rrfield[3] + \rrfield[5], \quad
\genparam = \param[0] + \param[2] + \param[4]
\end{align*}

\paragraph{Action}

\begin{align*} 
\int_7 \tr \paren{\field[1] \wedge \extd \rrfield[5] + \rrfield[5] \wedge \extd \field[1] + \rrfield[3] \wedge \extd \rrfield[3] +  2 \field[1] \wedge \field[1] \wedge \rrfield[5] + 2 \field[1] \wedge \rrfield[3] \wedge \rrfield[3]}
\end{align*}

\paragraph{Gauge transformations}

\begin{align*} 
\delta \field[1] = \extd \param[0] + \liebra{\field[1]}{\param[0]}, 
\ \delta \rrfield[3] = \extd \param[2] + \liebra{\rrfield[3]}{\param[0]} + \liebra{\field[1]}{\param[2]},
\\ \ \delta \rrfield[5] = \extd \param[4] + \liebra{\rrfield[5]}{\param[0]} + \liebra{\rrfield[3]}{\param[2]} + \liebra{\field[1]}{\param[4]}\qquad
\end{align*}

\paragraph{Equations of motions}

\begin{align*} 
\extd \field[1] + \frac{1}{2} \liebra{\field[1]}{\field[1]} =0, \quad
\extd \rrfield[3] + \liebra{\rrfield[3]}{\field[1]} =0
\quad \extd \rrfield[5] + \liebra{\rrfield[5]}{\field[1]}
+ \frac{1}{2} \liebra{\rrfield[3]}{\rrfield[3]} = 0
\end{align*}

\subsubsection{In nine dimension}
\label{sec:ggt-egs-diffform-only-dim-nine}
% nine dimensional choise
The choice here is
\begin{align*} 
\genfield = \rrfield[3] + \rrfield[5], \quad
\genparam = \param[2] + \param[4]
\end{align*}
This is a good illustration to show that  we do not need $1$-form in the action.  Of course we could have included $7$-form, and $1$-form.
%% nine dimensional example

\paragraph{Action}

\begin{align*} 
\int_9 \tr \paren{\rrfield[3] \wedge \extd \rrfield[5] + \rrfield[5] \wedge \extd \rrfield[3] + \frac{2}{3} \rrfield[3] \wedge \rrfield[3] \wedge \rrfield[3]}
\end{align*}

\paragraph{Gauge transformation}

\begin{align*} 
\delta \rrfield[3] = \extd \param[2], \quad \delta \rrfield[5] = \extd \param[4] + \liebra{\rrfield[3]}{\param[2]}
\end{align*}

\paragraph{Equations of motion}

\begin{align*} 
\extd \rrfield[3] = 0,\quad
\extd \rrfield[5] + \frac{1}{2} \liebra{\rrfield[3]}{\rrfield[3]} = 0, \quad \liebra{\rrfield[3]}{\rrfield[5]} = 0
\end{align*}

% \subsection{Differential forms with fermions}
% \label{sec:ggt-egs-diff-form-times-fermions}

% \begin{center}
%   to be written
% \end{center}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "ggt-20150828"
%%% End: 
