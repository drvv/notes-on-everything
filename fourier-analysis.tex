\newcommand{\Fourier}{\mathcal{F}}
\chapter{Fourier analysis}
\label{chap:fourier-analysis}

There are tons of things to care about to define the precise notion of Fourier analysis.  Mathematician would treat this theme in ``rigorous'' terminology developed by functional analysis.  Physicists on the other hand, prefer to use analogy between finiteness and infiniteness.  They do not dare care about ``subtle'' matters that originates from the difference of cardinalities, namely $\aleph_0$ and $\aleph_1$.  Working mathematician would not say yes to this treatment of course.  However, it is not always the rigour that brought a new sights to mathematics.  Following this belief, we will also want to see how Fourier analysis is being used.  We also summarise several conventions.

\section{Fourier transformation in physics}
\label{sec:fourier-transform-in-physics}

\subsection{Fourier transformation as an integral transformation}
\label{sec:fourier-transform-integral-transformation}

The ``definition'' \footnote{Mathematically speaking, this ``definition'' does not serve as it should.  Strictly speaking, you have to ask ``Is the integral well-defined always?  If not, when does it become well-defined?  Equivalently, what function can be Fourier transformed?''.  But as I wrote above, we would like to see how it is practically used.  Most physicists do not at all care about the possibility of this transformation.  I would like to state the situation as is.}
of Fourier transformation of a function $f$ is
\begin{equation}
  \label{eq:def-fourier-transform-in-physics}
  \tilde{f}(k) = \int_{-\infty}^{\infty} dx \, e^{-ikx} f(x)
\end{equation}
$\tilde{f}$ is called \emph{Fourier transform} of $f$.  When we want to make it clear that this is a \emph{map} from function to function, we use
\begin{equation*}
  \Fourier[f](k) \equiv \tilde{f}(k).
\end{equation*}
The minus sign of the exponential $e^{-ikx}$ might be somewhat puzzling at this moment.  It seems more natural that you give a plus sign for it.  But let us continue for the time being.  The reason for it will become clear soon.



The inverse Fourier transformation is defined in similar way:
\begin{equation}
  \label{eq:def-inverse-fourier-transform-in-physics}
  f(x) = \frac{1}{2\pi} \int_{-\infty}^{\infty} dk \, e^{ikx} \tilde{f}(k)
\end{equation}
Here, the $1/2\pi$ coefficient is required, so that the ``inverse transformation'' works as the name suggests.  $2\pi$ coefficient arises when doing the following integral.
\begin{equation}
  \label{eq:delta-function-normalisation}
  \int_{-\infty}^{\infty} dk\, e^{ik(x-x')} = 2\pi \delta(x-x').
\end{equation}
The conventions mentioned here is summarised in the table below \ref{tab:fourier-analysis-convention}.
\begin{table*}[t]
  \centering
  \begin{tabular}[h]{c|c}
    Fourier & Inverse Fourier\\[10pt]
%    \hline\\[5pt]
    $\displaystyle \tilde{f}(k) = \int_{-\infty}^{\infty} dx \, e^{-ikx} f(x)$ &   $\displaystyle f(x) = \frac{1}{2\pi} \int_{-\infty}^{\infty} dk \, e^{ikx} \tilde{f}(k)$\\[15pt]
    $\displaystyle \tilde{f}(k) = \frac{1}{\sqrt{2\pi}} \int_{-\infty}^{\infty} dx \, e^{-ikx} f(x)$ &$\displaystyle f(x) = \frac{1}{\sqrt{2\pi}} \int_{-\infty}^{\infty} dk \, e^{ikx} \tilde{f}(k)$\\[15pt]
    $\displaystyle \tilde{f}(k) =  \int_{-\infty}^{\infty} dx \, e^{-2\pi ikx} f(x)$ &$\displaystyle f(x) = \int_{-\infty}^{\infty} dk \, e^{2\pi ikx} \tilde{f}(k)$
  \end{tabular}
  \caption{Conventions of Fourier transform definition}
  \label{tab:fourier-analysis-convention}
\end{table*}

\subsection{Algebraic interpretation}
\label{sec:fourier-algebraic}

The equation \eqref{eq:delta-function-normalisation} can be transformed to the following form.
\begin{equation}
  \label{eq:exponential-orthonomality}
  \int_{-\infty}^{\infty} dk\, \paren{\frac{e^{ikx'}}{\sqrt{2\pi}}}^*\paren{\frac{e^{ikx}}{\sqrt{2\pi}}} = \delta(x-x').
\end{equation}
Now this equation can be seen as ``orthonomality'' condition for $\curly{\frac{e^{ikx}}{\sqrt{2\pi}}}_{k\in \reals}$, although ``norm'' of $\frac{e^{ikx}}{\sqrt{2\pi}}$ seems to be divergent $\delta(0)$.

Another convention is to take $\curly{e^{2\pi ikx}}_{k \in \reals}$ as a ``orthonormal basis'' for vector space of functions:
\begin{equation*}
  \int_{-\infty}^{\infty} dk\, \paren{{e^{2\pi ikx'}}}^*\paren{{e^{2\pi ikx}}} = \delta(x-x').
\end{equation*}
This is verified from \eqref{eq:exponential-orthonomality} by change of variable.

\subsubsection{As spectral decomposition of differential operator}
\label{sec:fourier-spectrum-decomposition}

Exponential function satisfies the following equation.
\begin{equation*}
  \diff{e^{\lambda x}}{x} = \lambda e^{\lambda x}.
\end{equation*}
This equation can be seen as an ``eigenvalue equation'' for $\diff{}{x}$ operator
\begin{equation*}
  \diff{}{x} e^{\lambda x} = \lambda e^{\lambda x},
\end{equation*}
where the eigenvalue is $\lambda$.

With this viewpoint, you might understand why we put minus sign for Fourier transform, and plus sign for inverse Fourier transform.  Inverse Fourier transform is now interpreted as an expansion with respect to an orthonormal basis $\curly{\frac{e^{ikx}}{\sqrt{2\pi}}}_{k\in \reals}$.
\begin{equation}
  \label{eq:inverse-fourier-transform-as-decomposition}
  f(x) = \int_{-\infty}^{\infty} dk \, \frac{e^{ikx}}{\sqrt{2\pi}}  \tilde{f}(k)
\end{equation}
Here, $k$-th ``vector'' is $\frac{e^{ikx}}{\sqrt{2\pi}}$, and its corresponding ``component'' is $\tilde{f}(k)$.  Let us use the following notation:
\begin{align*}
  \ket{e_k}_x &\equiv \frac{e^{ikx}}{\sqrt{2\pi}}\\
  \bra{e_k}_x &\equiv \frac{e^{-ikx}}{\sqrt{2\pi}}
\end{align*}
Then the orthonomality could be written
\begin{align*}
  \braket{e_k}{e_l} &= \sum_x \bra{e_k}_x \ket{e_l}_x\\
  &=\int dx \frac{e^{-ikx}}{\sqrt{2\pi}} \frac{e^{ilx}}{\sqrt{2\pi}}\\
  &= \delta(k-l)
\end{align*}
By virtue of ``orthonomality'' of the basis, getting a specific component is easily done.  That is, taking inner product of 

\subsection{Fourier analysis as a tool for ODE}
\label{sec:fourier-analysis-as-tool-for-ODE}

The birds-eye-view is the following diagram:
\begin{equation}
  \label{diag:fourier-analysis-birdseyeview}
  \xymatrix{%
    f(x) \ar[r]^{\mathcal{F}} \ar[d]^{\partial}& \tilde{f}(k) \ar[d]^{k \times} \\
    \displaystyle \pardiff{f}{x} & ik \tilde{f}(k) \ar[l]^{\mathcal{F}^{-1}}
  }
\end{equation}
Equivalently, the operators relation is as follows.
\begin{equation}
  \label{eq:differential-multiplication-duality}
  \pardiff{}{x} = \Fourier ^{-1} ik \Fourier,
\end{equation}
where $x$ is the variable of ``real space'' and $k$ of ``Fourier space'', ``wave number space'', or ``momentum space''.

\begin{example}
  \emph{Forced oscillator}
  \begin{align}
    \label{eq:fourier-analysis-eg-oscillator-real-space}
    \ddiff{x}{t} + \omega_0^2 x &= g(t).\\
    \diff{}{t}\diff{}{t} x + \omega_0^2 x &= g(t).
  \end{align}
  According to \eqref{eq:differential-multiplication-duality}, the differential operator is converted to a ``sandwich'' of Fourier transformation operators and multiplication in Fourier space.
  \begin{equation*}
    (\Fourier^{-1}i\omega\Fourier) (\Fourier^{-1}i\omega\Fourier) x + \omega_0^2 x(t) = g(t).
  \end{equation*}
  By Fourier transforming both sides,
  \begin{align*}
    \Fourier \sqbra{(\Fourier^{-1}i\omega\Fourier)(\Fourier^{-1}i\omega\Fourier) x + \omega_0^2x(t) } &= \Fourier g(t)\\
    (\Fourier \Fourier^{-1}) i\omega (\Fourier \Fourier^{-1} )i\omega\Fourier x + \omega_0^2 \Fourier x &= \Fourier g(t)\\
    (i\omega)^2 \tilde{x}(\omega) + \omega_0^2 \tilde{x}(\omega) &= \tilde{g}(\omega)
  \end{align*}
  The final line can be easily solved by division and we have
  \begin{equation}
    \label{eq:fourier-analysis-eg-oscillator-fourier-component}
    \tilde{x}(\omega) = \frac{\tilde{g}(\omega)}{\omega^2 - \omega_0^2}.
  \end{equation}
  Now we have got the expression for the Fourier transform of $x$.  Want the one in real space?  Then you get it by inverse Fourier transformation now:
  \begin{align*}
    \Fourier^{-1} \tilde{x}(\omega) &= \Fourier^{-1} \sqbra{\frac{\tilde{g}(\omega)}{\omega^2 - \omega_0^2}}\\
    \Fourier^{-1} (\Fourier{x}) &= \Fourier^{-1} \sqbra{\frac{\tilde{g}(\omega)}{\omega^2 - \omega_0^2}}\\
    x(t) &= \Fourier^{-1} \sqbra{\frac{\tilde{g}(\omega)}{\omega^2 - \omega_0^2}}.
  \end{align*}
  The explicit expression for $x$ is gained, writing $\Fourier^{-1}$ in integral:
  \begin{equation*}
    x(t) = \frac{1}{2\pi}\int_{-\infty}^{\infty} dt \, e^{i\omega t} \sqbra{\frac{\tilde{g}(\omega)}{\omega^2 - \omega_0^2}}.
  \end{equation*}
\end{example}

Inverse Fourier transformation can be seen as an expansion with respect to ``eigenvector''.  This notion is useful in solving ODE.  Fourier ``expansion'' brings ODE to algebraic equation for Fourier components.  Roughly speaking,
\begin{align*}
  \text{differentiation } \diff{}{x} &\to \text{multiplication } ik\\
  \text{function } f(x) &\to \text{Fourier component } \tilde{f}(k)  
\end{align*}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "master"
%%% End: 
