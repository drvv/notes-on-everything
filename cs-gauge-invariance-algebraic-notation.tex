\newcommand{\vara}{\delta A}  % gauge variance

We only use the algebraic properties of the $\liealg$-valued wedge and the wedged Killing form, such as graded (anti)symmetry and the invariance.  This is a crucial step to proceed to the construction of the generalised Chern-Simons action.

\subsection{Variance of CS action}
\label{sec:variance-of-cs-action}

Important note:  In terms of the wedged Killing form and the $\liealg$-valued wedge, the factor of the non abelian term is $\frac{1}{3}$, in stead of $\frac{2}{3}$, which is more popular.  This is because
\begin{equation}
  \label{eq:wedged-liebra-and-without-wedging-equivalence}
  A \wedge A = \half \liewedge{A}{A}.
\end{equation}

\subsubsection{Non abelian term}
\label{sec:cs-action-non-abelian-term}
The variance of non abelian term is
\begin{align}
  \label{eq:variance-cs-algebraic}
  \frac{1}{3}\paren{\killingwedge{\vara}{\liewedge{A}{A}} + \killingwedge{A}{\liewedge{\vara}{A}} + \killingwedge{A}{\liewedge{A}{\vara}}}.
\end{align}
By graded antisymmetry of wedged Killing form
\begin{align}
  \label{eq:cs-variance-wedged-killing-graded-antisymmetry}
  \liewedge{A}{\vara} &= -\parityfactor{A}{\vara} \liewedge{\vara}{A}\\
  &= \liewedge{\vara}{A},
\end{align}
and by the invariance
\begin{equation}
  \label{eq:cs-variance-wedged-killing-invariance}
  \killingwedge{\liewedge{A}{A}}{\vara} = \killingwedge{A}{\liewedge{A}{\vara}},
\end{equation}
the variance becomes
\begin{align}
  \label{eq:cs-variance-nonabelian-before-implementing}
   \killingwedge{\vara}{\liewedge{A}{A}}.
\end{align}

\paragraph{Implementing $\vara$}

\begin{align}
  \label{eq:cs-variance-nonabelian-implemented}
  & \killingwedge{\paren{\extd v + \liewedge{A}{v}}}{\liewedge{A}{A}} \\
  &= \killingwedge{\extd v}{\liewedge{A}{A}} +  \killingwedge{\liewedge{A}{v}}{\liewedge{A}{A}}.
\end{align}

The last term vanishes because of the invariance and the fact that
\begin{align}
  \label{eq:three-liewedge-vanishes}
  \liewedge{A}{\liewedge{A}{A}} = 0.
\end{align}
This is because of the graded Jacobi identity and the graded antisymmetry of wedged Lie bracket.  While, by graded Jacobi identity,
\begin{align}
  \label{eq:proof-three-liewedge-vanishes}
  \liewedge{\liewedge{A}{A}}{A} &= \liewedge{A}{\liewedge{A}{A}} - \parityfactor{A}{A} \liewedge{A}{\liewedge{A}{A}}\\
  &= 2 \liewedge{A}{\liewedge{A}{A}},
\end{align} the graded antisymmetry states
\begin{align}
  \liewedge{A}{\liewedge{A}{A}} &= -\parityfactor{A}{\liewedge{A}{A}} \liewedge{\liewedge{A}{A}}{A}\\
  &= -\liewedge{\liewedge{A}{A}}{A}.
\end{align}  Thus the term vanishes.  So far we have
\begin{align}
  \label{eq:cs-variance-nonabelian-sofar}
  \killingwedge{\extd v}{\liewedge{A}{A}}.
\end{align}

\subsubsection{Derivative term}
\label{sec:cs-action-derivative-term}

The variance of the derivative term is
\begin{align}
  \label{eq:cs-variance-derivative}
  \killingwedge{\vara}{\extd A} + \killingwedge{A}{\extd (\vara)}
\end{align}
By Leibniz rule
\begin{align}
  \label{eq:cs-variance-derivative-second-term-with-leibniz}
  \extd \killingwedge{A}{\vara} = \killingwedge{\extd A}{\vara} - \killingwedge{A}{\extd (\vara)},
\end{align}
and the graded symmetry, the variance becomes
\begin{align}
  \label{eq:cs-variance-derivative-before-implementation}
  2 \killingwedge{\vara}{\extd A} - \extd{\killingwedge{A}{\vara}}.
\end{align}

\paragraph{Implementing $\vara$}

\begin{align}
  \label{eq:cs-variance-derivative-implemented}
  2 \killingwedge{\paren{\extd v + \liewedge{A}{v}}}{\extd A} - \extd{\killingwedge{A}{\vara}}.
\end{align}
We leave the $\vara$ inside the exterior derivative.  Bilinearity implies
\begin{align}
  \label{eq:cs-variance-derivative-implemented-bilinear-expansion}
  2 \killingwedge{\extd v}{\extd A} + 2 \killingwedge{\liewedge{A}{v}}{\extd A} - \extd{\killingwedge{A}{\vara}}.
\end{align}
By the invariance and the graded symmetry, the second term can be written as
\begin{align}
  \label{eq:cs-variance-derivative-implemented-bilinear-expansion-second-term}
  \killingwedge{\liewedge{A}{v}}{\extd A} &= \parityfactor{\liewedge{A}{v}}{\extd A} \killingwedge{\extd A}{\liewedge{A}{v}}\\
  &= \killingwedge{\liewedge{\extd A}{A}}{v}\\
  &=\parityfactor{v}{\liewedge{\extd A}{A}} \killingwedge{v}{\liewedge{\extd A}{A}}\\
  &=\killingwedge{v}{\liewedge{\extd A}{A}}.
\end{align}
By the way, using Leibniz rule together with graded antisymmetry of the wedged Lie bracket,
\begin{align}
  \label{eq:cs-variance-derivative-implemented-bilinear-expansion-second-term-leibniz}
  \extd \liewedge{A}{A} &= \liewedge{\extd A}{A} - \liewedge{A}{\extd A}.\\
  &= 2 \liewedge{\extd A}{A}.\\
  \therefore \liewedge{\extd A}{A} &= \extd \paren{\half \liewedge{A}{A}}.
\end{align}
Thus we have 
\begin{align}
  \label{eq:cs-variance-derivative-final}
    2 \killingwedge{\extd v}{\extd A} + 2 \killingwedge{v}{\extd \paren{\half \liewedge{A}{A}}} - \extd{\killingwedge{A}{\vara}}.
\end{align}

\subsubsection{Combined result}
\label{sec:cs-variance-combined}

Combining the results \eqref{eq:cs-variance-nonabelian-sofar} and \eqref{eq:cs-variance-derivative-final}, we get
\begin{align}
  \label{eq:cs-results-combined}
  &2 \killingwedge{\extd v}{\extd A} + 2 \killingwedge{v}{\extd \paren{\half \liewedge{A}{A}}} - \extd{\killingwedge{A}{\vara}} + \killingwedge{\extd v}{\liewedge{A}{A}}\\
  &= 2\extd \killingwedge{v}{\extd A} + \extd \killingwedge{v}{\liewedge{A}{A}} - \extd{\killingwedge{A}{\vara}}\\
  &= 2\extd \killingwedge{v}{\paren{\extd A + \half \liewedge{A}{A}}} - \extd{\killingwedge{A}{\vara}}\\
  &= 2\extd \killingwedge{v}{F} - \extd{\killingwedge{A}{\vara}}\\
  &= 2\extd \killingwedge{v}{F} - \extd{\killingwedge{A}{\extd v + \liewedge{A}{v}}}\\
  &= 2\extd \killingwedge{v}{F} - \paren{\extd{\killingwedge{A}{\extd v}}+ \killingwedge{A}{\liewedge{A}{v}}}\\
  &= 2\extd \killingwedge{v}{F} - \paren{\extd{-\killingwedge{\extd v}{A}}+ \killingwedge{\liewedge{A}{A}}{v}}\\
  &= 2\extd \killingwedge{v}{F} - \paren{\extd(-\extd \killingwedge{v}{A} + \killingwedge{v}{\extd A}) + \killingwedge{v}{\liewedge{A}{A}}}\\
  &= 2\extd \killingwedge{v}{F} - \paren{\extd(\killingwedge{v}{\extd A}) + \killingwedge{v}{\liewedge{A}{A}}}
\end{align}
where $F = \extd A + \half \liewedge{A}{A}$ is the curvature form.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "notes-on-ggt-20150731.tex"
%%% End: 
