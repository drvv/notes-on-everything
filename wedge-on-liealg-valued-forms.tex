\newcommand{\liegen}[1][\mbox{}]{T^{#1}}

\subsection{Definition of $\liealg$-valued wedge}
\label{sec:liealg-valued-wedge}

The algebraic problem of this space lies in the multiplication on this space.  In principle Lie algebra does not  define multiplication on it, while differential forms have anticommutative multiplication called ``wedge''.

A straightforward idea is the following:  Combine two operations at once!
\begin{equation}
  \label{eq:def-wedge-on-liealg-valued-diff-forms-preliminary}
  T \lambda \times T'\lambda' \mapsto \liebra{T}{T'} \lambda \wedge \lambda'.
\end{equation}
We always want bilinearity of this kind of pairs.  This is equivalent to saying that we consider the tensor product of these spaces $\liealg$ and $\Lambda$.  Thus we arrive at the definition of ``wedge on forms''.

%\newcommand{\liewedge}[2]{\sqbra{#1 \wedge #2}}
\begin{definition}[Wedge on $\liealg$-valued forms]
  The bilinear extension of the following binary operation $\liewedge{\ }{\ }: (\liealg \tensor \Lambda) \tensor (\liealg \tensor \Lambda) \to \liealg \tensor \Lambda$ is called $\liealg$-valued wedge.   Let $A = a \tensor \alpha$ and $B = b \tensor \beta$, where $a,b \in \liealg$ and $\alpha, \beta \in \Lambda$.
  \begin{equation}
    \label{eq:def-wedge-on-liealg-valued-diff-forms}
    \liewedge{A}{B} = \liebra{a}{b} \tensor \alpha \wedge \beta.
  \end{equation}
  % on $\liealg$-valued form is the linear extension of the following operation.
\end{definition}
By ``bilinear extension'' we mean that the operator has bilinearity in terms of scalar multiplication.  To be explicit, we have
\begin{align}
  \label{eq:def-wedge-on-liealg-valued-diff-forms-linear-extended}
  \liewedge{A}{B} &= \liewedge{A^c_\mu T^c \tensor dx^\mu}{B^d_\nu T^d \tensor dx^\nu }\\
  &= A^c_\mu B^d_\nu \liewedge{ T^c \tensor dx^\mu}{ T^d \tensor dx^\nu }\\
  &= A^c_\mu B^d_\nu \liebra{\liegen[c]}{\liegen[d]} \tensor dx^\mu \wedge dx^\nu 
\end{align}

\subsection{Algebraic properties of $\liealg$-valued wedge}
\label{sec:algebraic-properties-of-liealg-valued-wedge}

The following table summarises the main content of this section.
\begin{table}[h]
  \centering
  \begin{tabular}[h]{r || c | c}
    space & $\liealg$ & $\liealg \tensor \Lambda$ \\
    operation & $\liebra{\ }{\ }$ & $\liewedge{\ }{\ }$\\
    symmetry &
    $\begin{matrix}
      \text{antisymmetric}\\
      \liebra{A}{B} = -\liebra{B}{A}
    \end{matrix}$
    & $
    \begin{matrix}
      \text{\emph{graded} antisymmetric}\\
      \liewedge{A}{B} = -\parityfactor{A}{B}\liewedge{B}{A}
    \end{matrix}$ \\
    adjoint & Lie algebra homomorphism & \emph{graded} homomorphism\\
    Jacobi & (normal) Jacobi & \emph{graded} Jacobi
  \end{tabular}
  \caption{Algebraic properties of $\liealg$-valued wedge and the comparison}
  \label{tab:algebraic-properties-of-liealg-valued-wedge}
\end{table}
The parity (or degree) $\abs{A}$ of an element $A \in \liealg \tensor \Lambda$ is defined as the parity (or degree) of its $\Lambda$ part.  Namely, 
\begin{equation}
  \label{eq:parity-of-liealg-valued-form}
  \abs{A} = \abs{a \tensor \alpha} := \abs{\alpha}.
\end{equation}

\subsubsection{Graded antisymmetry}
\label{sec:liealg-valued-wedge-graded-antisymmetry}

Graded antisymmetry directly follows from the definition, this is due to the graded antisymmetry of exterior algebra $\Lambda$.

\subsubsection{Graded Jacobi identity}
\label{sec:liealg-valued-wedge-graded-jacobi}

The \emph{graded} Jacobi identity is inherited from the original Jacobi identity in $\liealg$.  The origin of the factor is again the grading structure in the exterior algebra $\Lambda$.  We will show this explicitly in what follows.  We assume that elements are factorised in the proof, and obviously this arguments extends to the general case.

\begin{align}
  \label{eq:proof-graded-jacobi-identity}
  &\liewedge{A}{\liewedge{B}{C}}\\
  &=\liewedge{a \tensor \alpha}{\liewedge{b \tensor \beta}{c \tensor \gamma}}\\
  &=\liebra{a}{\liebra{b}{c}} \tensor (\alpha \wedge \beta \wedge \gamma),\\
  \intertext{by the original Jacobi identity in $\liealg$,}
  &=\paren{-\liebra{b}{\liebra{c}{a}} -\liebra{c}{\liebra{a}{b}}} \tensor (\alpha \wedge \beta \wedge \gamma)\\
  &=\paren{\liebra{b}{\liebra{a}{c}} + \liebra{\liebra{a}{b}}{c}} \tensor (\alpha \wedge \beta \wedge \gamma)\\
  &=\liebra{b}{\liebra{a}{c}} \tensor \parityfactor{\alpha}{\beta} \beta \wedge \alpha \wedge \gamma + \liebra{\liebra{a}{b}}{c} \tensor (\alpha \wedge \beta \wedge \gamma)\\
  \label{eq:graded-jacobi-with-arg}
  &=\parityfactor{\alpha}{\beta}\liewedge{B}{\liewedge{A}{C}} + \liewedge{\liewedge{A}{B}}{C} \qed
%  \intertext{by the original antisymmetry in $\liealg$,}
%  &=\liebra{b}{\liebra{a}{c}} \beta \wedge \gamma \wedge \alpha - \liebra{c}{\liebra{a}{b}} \tensor (\alpha \wedge \beta \wedge \gamma)% \\
  % &=\liebra{a}pha}{\liewedge{b \tensor \beta}{c \tensor \gamma}}\\  
  % - \liewedge{B}{\liewedge{A}{C}}\\
  % \liewedge{\liewedge{A}{B}}{C} &= 
\end{align}
The last line implies that the adjoint map in ``$\Lambda$-valued Lie algebra'' ($\liealg \tensor \Lambda$) is a \emph{graded homomorphism}.  Define the adjoint map $\widehat{\text{ad}}$ as
\newcommand{\adwedge}[1]{\widehat{\text{ad}}_{#1}}
\begin{equation}
  \label{eq:def-adjoint-in-lambda-valued-liealg}
  \adwedge{A}(B) = \liewedge{A}{B}.
\end{equation}
We can easily see that $\adwedge{A}$ is a graded map of degree $\abs{A}$.

The graded Jacobi identity \eqref{eq:graded-jacobi-with-arg} implies
\begin{equation}
  \label{eq:graded-homomorphism}
  \adwedge{\liewedge{A}{B}} = \adwedge{A} \circ \adwedge{B} - \parityfactor{A}{B} \adwedge{B} \circ \adwedge{A}.
\end{equation}
The right hand side could be written with \emph{graded/super commutator}.  Therefore the adjoint map $\adwedge{} : \liealg \tensor \Lambda \to \eendo{\liealg \tensor \Lambda}$ is a graded homomorphism of degree $0$.

\subsubsection{Leibniz rule for $\liealg$-valued wedge}
\label{sec:Leibniz-rule-for-liealg-valued-wedge}

We often consider chain complexes $(\Lambda, d)$.  We can verify the \emph{Leibniz rule} for $\liewedge{\ }{\ }$ by the straightforward calculation.
\begin{equation}
  \label{eq:liealg-valued-wedge-and-exterior-derivative}
  \extd \liewedge{A}{B} = \liewedge{\extd A}{B} + (-1)^{\abs{A}} \liewedge{A}{\extd B}.
\end{equation}
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "notes-on-ggt-20150731.tex"
%%% End: 
