%% writing "Group Theory"
Notes on \emph{Algebra (abstract algebra).}

% \section{Linear Algebra}
% \label{sec:linear-algebra}

% \input{linear-algebra}

\section{Group Theory}
\label{sec:group-theory}

\subsection{Subgroup}
\label{sec:subgroup}

A certain subset of a group $G$ may also have the same group structure as $G$.  This is called a \textbf{subgroup}.
\begin{definition}[Subgroup]
  Let $H$ be a subset of a group $(G,\cdot)$.  If $(H,\cdot)$ forms a group, then $H$ is called a \textbf{subgroup} of $G$.  Namely, for arbitrary element $h,h'$ of $H$, the following conditions are required:
  \begin{gather}
    \label{eq:def-subgroup}
      h h' \in H\\
%      \exists e_{H} \text{ s.t. } h e_{H} = e_{H} h = h\\
      h\inverse \in H
  \end{gather}
\end{definition}

% The unit in a subgroup is inherited from the parent group.
% \begin{proposition}[Unit in subgroup]
%   Let $e_{H}$ be the unit of a subgroup $H \subset G$, and $e_{G}$ be the unit in the group $G$.  Then they are actually equal:
%   \begin{equation}
%     \label{eq:equality-unit-in-subgroup}
%     e_{H} = e_{G}.
%   \end{equation}
% \end{proposition}
% \begin{proof}
%   By construction, every element $h$ of $H$ is an element of the group $G$.  Therefore
%   \begin{equation*}
%     \forall h, \quad h e_{G} = e_{G} h = h.
%   \end{equation*}
%   Thus, by definition, $e_{G}$ is an unit in $H$.  The uniqueness of unit concludes the proof.
% \end{proof}

One does not have to check if all the axioms of group holds for the subgroup.
\begin{proposition}[Criterion of subgroup]
  Let $H$ be a subset of a group $G$.  $H$ is a subgroup if and only if
  \begin{equation}
    \label{eq:subgroup-criterion}
    \forall h_1, h_2 \in H, \quad h_1 h_2\inverse \in H.
  \end{equation}
\end{proposition}
\begin{proof}
  If $H$ is a subgroup of $G$, then by definition, for every $h \in H$, $h\inverse \in H$ and the product is closed.  Thus  $h_1 h_2\inverse \in H$.

  \begin{itemize}
  \item (The existence of unit)
    If every $h_1, h_2 \in H$ satisfies the condition above \eqref{eq:subgroup-criterion}, set $h_2 = h_1$. It gives
    \begin{equation*}
      H \ni h_1 h_1\inverse = e_{G}.
    \end{equation*}
    Thus the unit in $G$ is an element of $H$.  By the uniqueness of unit, the unit $e_{G}$ of $G$ is also the unit of $H$.
  \item (The existence of inverse)
    It follows from above argument that the unit of $G$ is an element of $H$.  Therefore, for every $h \in H$,
    \begin{equation*}
      H \ni e_{G} h\inverse = h\inverse
    \end{equation*}
    Thus the inverse $h\inverse$ is an element of $H$.
  \item (Closure)
    It follows from above argument that the inverse of an element in $H$ is also an element of $H$.  Thus for any two elements $h_1,h_2 \in H$,
    \begin{equation*}
      h_1 h_2 = h_1 (h_2 \inverse)\inverse \in H.
    \end{equation*}
  \end{itemize}
\end{proof}

\subsection{Homomorphism}
\label{sec:homomorphism}
\newcommand{\omicron}{o}

We omit the multiplication symbols which are defined in groups, and use the same symbol for inverse in possibly different groups.

\textbf{Homomorphism} ($\acute{\omicron}\mu\omicron\sigma + \mu\omicron\rho\phi\acute{\eta}$) is a map that preserves the group structure.  Formally,

\begin{definition}[Homomorphism]
  Let $\phi$ be a map between groups; $\phi: G \to G'$.  Then $\phi$ is \textbf{homomorphism} if
  \begin{equation}
    \label{eq:def-homomorphism}
    \phi(g_1 g_2) = \phi(g_1) \phi(g_2).
  \end{equation}
  for any elements $g_1,g_2 \in G$.
\end{definition}
Some properties of homomorphism straightforwardly follows from the definition.
\begin{proposition}
  \begin{enumerate}
  \item the unit $e \in G$ is mapped to the other unit $e' \in G'$:
    \begin{equation}
      \label{eq:prop-homomorphism-unit}
      \phi(e) = e'
    \end{equation}
  \item the inverse element $g\inverse \in G$ of $g$ is mapped to the corresponding inverse $\phi(g)\inverse \in G'$
    \begin{equation}
      \label{eq:prop-homomorphism-inverse}
      \phi(g\inverse)  = \phi(g) \inverse
    \end{equation}
  \end{enumerate}
\end{proposition}

\begin{proof}
  \begin{enumerate}
  \item
    \begin{align*}
      \phi(ge) &=\phi(g)\\
      \phi(g)\phi(e) &= \phi(g)
      \intertext{Since $\phi(g) \in G'$, it has the inverse in $G'$:}
      \phi(g)\inverse \phi(g) \phi(e) &= \phi(g)\inverse \phi(g)\\
      \therefore \phi(e) &= e' \in G'.
    \end{align*}
  \item
    In the same way,
    \begin{align*}
      \phi(gg\inverse ) &= \phi(e)\\
      \phi(g)\phi(g\inverse ) &= e'
      \intertext{Multiply the both sides by $\phi(g)\inverse$ }
      \phi(g\inverse) &= \phi(g)\inverse e' \\
      \therefore \phi(g\inverse) &= \phi(g)\inverse .
    \end{align*}
  \end{enumerate}
\end{proof}

Given a map, it is natural to ask if one ``output'' is ``shared'' by more than two different ``inputs''.  Formally, this leads to the definition of \textbf{injection}.
\begin{definition}[Injection]
  Let $f$ be a map (not necessarily a group homomorphism).  $f$ is \textbf{injective}, an \textbf{injection}, or \textbf{one-to-one} if 
  \begin{equation}
    \label{eq:def-injection}
    f(a) = f(b) \implies a = b.
  \end{equation}
\end{definition}
If a map is not an injection, it is natural to classify elements in the domain (``inputs'') according to the image (``destination'').  In general, the ``classification'' of elements is achieved by introducing a binary relation on the domain of a map $f$ in the following, natural way.
\begin{definition}
  Let $f$ be a map between sets $f: A \to B$.  Define a binary relation $\sim$ such that 
  \begin{equation}
    \label{eq:def-similar-map}
    a \sim b \defarrow f(a) = f(b).
  \end{equation}
\end{definition}
It serves as a classification of elements because this relation is actually an equivalence relation:
\begin{proposition}
  The above binary relation \eqref{eq:def-similar-map} is equivalence relation.  The proof is solely dependent on the fact that the equality '$=$' is an equivalence relation.
\end{proposition}
\begin{proof}
  \begin{enumerate}
  \item Reflexivity
    \begin{equation*}
      a \sim a, \quad \because f(a) = f(a).
    \end{equation*}
  \item Symmetry
    \begin{gather*}
      a \sim b \ \implies \ b \sim a, \\
      \because f(a) = f(b) \implies f(b) = f(a).
    \end{gather*}
  \item Transitivity
    \begin{gather*}
      a \sim b \text{ and } b \sim c \implies a \sim c, \\
      \because f(a) = f(b) \text{ and } f(b) = f(c) \\
      \implies f(a) = f(c).
    \end{gather*}
  \end{enumerate}
\end{proof}
For group homomorphisms, there exist a powerful notion regarding this classification.  First we define the \textbf{kernel} of group homomorphism, which is a subset of the group whose elements are mapped to the unit.
\begin{definition}[Kernel]
  Let $\phi$ be a group homomorphism $\phi: G \to G'$.  The \textbf{kernel} of $\phi$, denoted by $\ker{\phi}$ is a subset of $G$ defined by
  \begin{equation}
    \label{eq:def-kernel}
    \ker{\phi} := \curly{g \in G \vert \phi(g) = e'}.
  \end{equation}
\end{definition}
One of the reason why this notion is useful lies in the following fact:
\begin{proposition}[Triviality of kernel and injection]
  For group homomorphism $\phi$, the following two statements are equivalent:
  \begin{enumerate}
  \item $\phi$ is injective
  \item $\ker\phi$ is trivial
  \end{enumerate}
\end{proposition}
Before moving on to the proof, we should note that kernel is actually a sub\emph{group} in the group.
\begin{proposition}
  % Let $\phi$ be a group homomorphism.  Then
  $\ker\phi$ is subgroup of $G$.
\end{proposition}
\begin{proof}
  Let $h_1, h_2$ be arbitrary elements in $\ker\phi$.  Since $\phi$ is a homomorphism,
  \begin{gather*}
    \phi(h_1h_2\inverse ) = \phi(h_1)\phi(h_2\inverse ) \\
    \therefore e' (e')\inverse = e'.
  \end{gather*}
  % Thus, $\ker\phi$ is a subgroup$h_1h_2 \in \ker\phi$.
\end{proof}
\begin{proof}[of the equivalence above]
  Suppose $h_1, h_2$ are arbitrary elements in $\ker\phi$:
  \begin{gather*}
    \phi(h_1) = \phi(h_2) = e'
    \intertext{If $\phi$ is injective,}
    \phi(g_1) = \phi(g_2) \implies \phi(g_1 g_2\inverse ) = e'.
    \intertext{If $\phi$ is injective,}
    g_1 = g_2.  \therefore \phi(e) = e'.
  \end{gather*}
  Let $a, b \in G$ 

  If the kernel is trivial; $\ker\phi = \curly{e}$,
  \begin{gather*}
    \phi(ab\inverse) = e \implies \quad ab\inverse = e.\\
    \therefore a = b.
  \end{gather*}

  If $\phi$ is injective, 
  \begin{gather*}
    \phi(ab\inverse) = e \implies ab\inverse = e\\
    \equivalent \phi(a) = e \implies a = e\\
    \equivalent a \in \ker\phi \implies a = e.
  \end{gather*}
\end{proof}

\input{function-as-vector}

\input{gradings}
% \section{Quaternion}
% \label{sec:quaternion}

% \input{quaternions}

% \section{Grassmann / Exterior Algebra}
% \label{sec:grassmann-algebra}

\input{grassmann}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "master"
%%% End: 
